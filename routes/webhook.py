from tornado.web import RequestHandler
from utils.err import DiscordErrorReport
from tornado.httpclient import AsyncHTTPClient, HTTPRequest, HTTPError
from requests_toolbelt.multipart.encoder import MultipartEncoder
from tornado.escape import json_decode, json_encode
from utils import build_query, eval_expr, urldecode
from tornado.ioloop import IOLoop
from logging import Logger
from typing import Optional
import re


class WebhookHandler(DiscordErrorReport, RequestHandler):
    client = None

    def initialize(
        self,
        lang_cache: dict,
        log: Logger,
        bot_key: str,
        discord_report: str,
    ):
        self.lang_cache = lang_cache
        self.log = log
        self.bot_key = bot_key
        self.set_discord_webhook(discord_report)

    @classmethod
    def prepare(cls):
        if cls.client is None:
            AsyncHTTPClient.configure("tornado.curl_httpclient.CurlAsyncHTTPClient")
            cls.client = AsyncHTTPClient(
                force_instance=True,
                defaults=dict(
                    user_agent="TelegramBot/v1.0.0-QueryMe",
                ),
            )

    async def send_action(self, chat_id: int, action: str):
        try:
            await self.client.fetch(
                f"https://api.telegram.org/bot{self.bot_key}/sendChatAction?"
                + build_query(
                    chat_id=chat_id,
                    action=action,
                )
            )
        except HTTPError as e:
            raise RuntimeError(
                f"HTTP error {e.response.code}: {e.response.body.decode()}"
            )

    async def cancel_action(self, chat_id):
            try:
                await self.send_action(chat_id, 'cancel')
            except Exception as e:
                self._handle_request_exception(e)

    async def send_message(self, chat_id: int, msg: str, **extra_args):
        try:
            await self.client.fetch(
                f"https://api.telegram.org/bot{self.bot_key}/sendMessage?"
                + build_query(
                    chat_id=chat_id,
                    parse_mode="markdown",
                    text=msg,
                    **extra_args,
                )
            )
        except HTTPError as e:
            raise RuntimeError(
                f"HTTP error {e.response.code}: {e.response.body.decode()}"
            )

    async def send_photo(self, chat_id: int, caption: str, photo: bytes, mime: str):
        code = {
            "image/png": "png",
            "image/jpg": "jpg",
            "image/ico": "ico",
            "image/gif": "gif",
        }.get(mime, "img")

        body = MultipartEncoder(
            fields={
                "chat_id": str(chat_id),
                "caption": caption,
                "parse_mode": "markdown",
                "photo": (f"photo.{code}", photo, mime),
            }
        )

        req = HTTPRequest(
            url=f"https://api.telegram.org/bot{self.bot_key}/sendPhoto",
            method="POST",
            body=body.read(),
            headers={"Content-Type": body.content_type},
        )

        try:
            await self.client.fetch(req)
        except HTTPError as e:
            raise RuntimeError(
                f"HTTP error {e.response.code}: {e.response.body.decode()}"
            )

    async def search_wikipedia(self, term: str, lang: str = "en") -> Optional[str]:
        try:
            res = await self.client.fetch(
                f"https://{lang}.wikipedia.org/w/api.php?"
                + build_query(
                    format="json",
                    action="query",
                    prop=b"extracts&exintro&explaintext",
                    redirects="1",
                    titles=term,
                )
            )
        except HTTPError as e:
            raise RuntimeError(
                f"HTTP error {e.response.code}: {e.response.body.decode()}"
            )
        else:
            data = json_decode(res.body.decode())
            pages = data["query"]["pages"]
            if "-1" in pages:
                return None

            return [pages[idx]["extract"] for idx in pages.keys()]

    async def search_duckduckgo(self, term: str, lang: str = "en-US") -> Optional[dict]:
        req = HTTPRequest(
            url="https://api.duckduckgo.com/?"
            + build_query(
                q=term,
                format="json",
                pretty="0",
                no_redirect="1",
                no_html="1",
            ),
            method="GET",
            headers={
                "Accept-Language": lang,
            },
            follow_redirects=True,
        )

        res = None
        try:
            res = await self.client.fetch(req)
        except HTTPError as e:
            raise RuntimeError(
                f"HTTP error {e.response.code}: {e.response.body.decode()}"
            )
        data = json_decode(res.body.decode())
        parsed_data = dict()
        if data["AbstractText"] != "":
            parsed_data["description"] = data["AbstractText"]
            if "content" in data["Infobox"] and len(data["Infobox"]["content"]) != 0:

                def wp(el: dict) -> str:
                    if el["data_type"] in ["string", "official_website"]:
                        return "*%(label)s*: %(value)s" % el
                    elif el["data_type"] == "twitter_profile":
                        return "*%(label)s*: https://twitter.com/%(value)s" % el
                    elif el["data_type"] == "instagram_profile":
                        return "*%(label)s*: https://instagram.com/%(value)s" % el
                    elif el["data_type"] == "facebook_profile":
                        return "*%(label)s*: https://www.facebook.com/%(value)s" % el
                    elif el["data_type"] == "youtube_channel":
                        return "*%(label)s*: https://youtube.com/channel/%(value)s" % el
                    elif el["data_type"] == "soundcloud_id":
                        return "*%(label)s*: https://soundcloud.com/%(value)s" % el
                    elif el["data_type"] == "spotify_artist_id":
                        return (
                            "*%(label)s*: https://open.spotify.com/artist/%(value)s"
                            % el
                        )
                    elif el["data_type"] in [
                        "imdb_id",
                        "google_play_artist_id",
                        "itunes_artist_id",
                        "instance",
                        "rotten_tomatoes",
                    ]:
                        return None
                    else:
                        return (
                            "*%(label)s*: %(value)s (Parse incomplete for `%(data_type)s`)"
                            % el
                        )

                parsed_data["info_list"] = filter(
                    lambda x: x is not None, map(wp, data["Infobox"]["content"])
                )

        if len(data["RelatedTopics"]) != 0:
            def wp(el: dict) -> str:
                try:
                    return urldecode(
                        el['FirstURL']
                        .split('&')[0]
                        .replace('https://duckduckgo.com/', '')
                        .replace('c/', '')
                        .replace('_', ' ')
                        .replace('[', ' ')
                        .replace(']', ' ')
                    )
                except:
                    return None
            parsed_data['related'] = filter(lambda x: x is not None, map(wp, data["RelatedTopics"]))

        if len(data["Results"]) != 0:

            def wp(el: dict) -> str:
                return "- [%(Text)s](%(FirstURL)s)" % el

            parsed_data["links"] = map(wp, data["Results"])

        if len(parsed_data) == 0:
            return None

        return parsed_data

    async def download_images(self, url: str) -> Optional[bytes]:
        try:
            res = await self.client.fetch(url)
        except HTTPError as e:
            raise RuntimeError(
                f"HTTP error {e.response.code}: {e.response.body.decode()}"
            )
        else:
            if res.code != 200:
                return None, None
            return res.body, res.headers["Content-Type"]

    async def search_images_duckduckgo(
        self, term: str, lang: str = "en-US", limit: int = 5
    ) -> Optional[list]:
        req = HTTPRequest(
            url="https://api.duckduckgo.com/?"
            + build_query(
                q=term,
            ),
            method="GET",
            headers={
                "Accept-Language": lang,
            },
            follow_redirects=True,
        )

        res = None
        try:
            res = await self.client.fetch(req)
        except HTTPError as e:
            raise RuntimeError(
                f"HTTP error {e.response.code}: {e.response.body.decode()}"
            )
        vqd = re.compile("vqd=\\'(?P<vqd>.+)\\';").findall(res.body.decode())
        if len(vqd) == 0:
            return None
        else:
            vqd = vqd[0]

        req = HTTPRequest(
            url="https://duckduckgo.com/i.js?"
            + build_query(q=term, o="json", vqd=vqd, f=",,,,,", p=1),
            method="GET",
            headers={
                "Accept-Language": lang,
            },
            follow_redirects=True,
        )

        try:
            res = await self.client.fetch(req)
        except HTTPError as e:
            raise RuntimeError(
                f"HTTP error {e.response.code}: {e.response.body.decode()}"
            )
        data = json_decode(res.body.decode())

        def wp(el):
            return {
                "title": "*(Source: %(source)s)* %(title)s" % el,
                "url": el["thumbnail"],
            }

        return list(map(wp, data["results"][:limit]))

    async def google_translate(
        self, source_lang: str, target_lang: str, text: str
    ) -> Optional[str]:
        try:
            res = await self.client.fetch(
                "https://translate.googleapis.com/translate_a/single?"
                + build_query(
                    client="gtx",
                    action="query",
                    dt="t",
                    sl=source_lang.lower(),
                    tl=target_lang.lower(),
                    q=text,
                )
            )
        except HTTPError as e:
            raise RuntimeError(
                f"HTTP error {e.response.code}: {e.response.body.decode()}"
            )

        try:
            data = json_decode(res.body.decode())
            return data[0][0][0]
        except Exception:
            return "*Warning:* Google Translate API detect unusual traffic, try again later (2 - 48 hours)"

    def get(self):
        self.set_header("Location", "https://t.me/QueryThingsBot")
        self.set_status(308)
        self.finish()

    async def post(self):
        try:
            data = json_decode(self.request.body)
            self.log.info(json_encode(data))
            if "message" not in data:
                self.set_status(202)
                return self.finish()

            cmd = data["message"].get("text", "/help")
            params = cmd.split(" ")[1:]
            chat_id = data["message"]["chat"]["id"]
            lang = self.lang_cache.get(
                chat_id, data["message"]["from"]["language_code"]
            )

            action_await = self.send_action(chat_id, 'typing')
            if cmd.startswith("/start"):
                self.log.info(
                    'New chat started with "%s"', data["message"]["from"]["username"]
                )
                await self.send_message(chat_id, "Welcome user to QueryMe.")
            elif cmd.startswith("/help"):
                await self.send_message(
                    chat_id,
                    "\n".join(
                        [
                            "/start - Start chat",
                            "/help - Show help",
                            "/setlang - Set your custom language for search",
                            "/searchwiki - Search with WikiPedia",
                            "/searchduck - Search with DuckDuckGo",
                            "/search - Alias for /searchduck",
                            "/calc - Evaluate arithmetic expression",
                            "/image - Search an image",
                            "/translate - Translate words",
                        ]
                    ),
                )
            elif cmd.startswith("/setlang"):
                if len(params) != 1:
                    await self.send_message(
                        chat_id,
                        "*Param error:* Parameters count is not valid, Example: /setlang en-US",
                    )
                else:
                    self.lang_cache[chat_id] = params[0]
                    await self.send_message(chat_id, f"New language is `{params[0]}`")
            elif cmd.startswith("/searchwiki"):
                if len(params) == 0:
                    await self.send_message(
                        chat_id,
                        "*Param error:* Parameters count is not valid, Example: /searchwiki Hot Dog",
                    )
                else:
                    res = await self.search_wikipedia(
                        " ".join(params), lang.split("-")[0]
                    )
                    if res == None:
                        await self.send_message(
                            chat_id,
                            "*Search Not Found:* The query was not found, check and try again",
                        )
                    else:
                        for i in range(len(res)):
                            await self.send_message(
                                chat_id, f"*Result number {i + 1}*:"
                            )
                            for p in map(lambda s: s.strip(), res[i].split("\n")):
                                if p != "":
                                    await self.send_message(chat_id, p)

            elif cmd.startswith("/search") or cmd.startswith("/searchduck"):
                if len(params) == 0:
                    await self.send_message(
                        chat_id,
                        "*Param error:* Parameters count is not valid, Example: /searchduck facebook",
                    )
                else:
                    res = await self.search_duckduckgo(" ".join(params), lang)
                    if res == None:
                        await self.send_message(
                            chat_id,
                            "*Search Not Found:* The query was not found, check and try again",
                        )
                    else:
                        if "description" in res:
                            await self.send_message(
                                chat_id, "*Short result:* " + res["description"]
                            )
                        if "info_list" in res:
                            await self.send_message(
                                chat_id,
                                "*Information list:*\n\n" + "\n".join(res["info_list"]),
                                disable_web_page_preview=True,
                            )
                        if "links" in res:
                            await self.send_message(
                                chat_id,
                                "*Related links:*\n\n" + "\n".join(res["links"]),
                                disable_web_page_preview=True,
                            )
                        if "related" in res:
                            data = map(lambda s: f'- `/searchduck {s}`', res["related"])
                            await self.send_message(
                                chat_id,
                                "*Related topics:*\n\n" + "\n".join(data),
                                disable_web_page_preview=True,
                            )
            elif cmd.startswith("/translate"):
                if len(params) < 3:
                    await self.send_message(
                        chat_id,
                        "*Param error:* Parameters count is not valid, Example: /translate ES EN hola",
                    )
                else:
                    res = await self.google_translate(
                        params[0], params[1], " ".join(params[2:])
                    )
                    if res == None:
                        await self.send_message(
                            chat_id,
                            "*Search Not Found:* The query was not found, check and try again",
                        )
                    else:
                        await self.send_message(chat_id, res)
            elif cmd.startswith("/image"):
                if len(params) == 0:
                    await self.send_message(
                        chat_id,
                        "*Param error:* Parameters count is not valid, Example: /image Nike",
                    )
                else:
                    res = await self.search_images_duckduckgo(" ".join(params), lang)
                    if res == None:
                        await self.send_message(
                            chat_id,
                            "*Search Not Found:* The query was not found, check and try again",
                        )
                    else:
                        for img in res:

                            async def wrapper(self, img, chat_id):
                                action_await = self.send_action(chat_id, 'upload_photo')
                                body, mime = await self.download_images(img["url"])
                                if body != None:
                                    await self.send_photo(
                                        chat_id, img["title"], body, mime
                                    )
                                await action_await
                                await self.cancel_action(chat_id)

                            IOLoop.current().spawn_callback(wrapper, self, img, chat_id)

            elif cmd.startswith("/calc"):
                if len(params) == 0:
                    await self.send_message(
                        chat_id,
                        "*Param error:* Parameters count is not valid, Example: /calc 1+1",
                    )
                else:
                    data = " ".join(params)
                    await self.send_message(
                        chat_id, "*Result is:* `%s`" % (eval_expr(data),)
                    )
            else:
                await self.send_message(
                    chat_id, "*Bad command:* write /help for show the help message"
                )

            await action_await

            
            IOLoop.current().spawn_callback(self.cancel_action, chat_id)

            self.set_status(204)
            self.finish()
        except Exception as e:
            await self.send_message(
                chat_id, "*Error:* An error was occurred, we are now trying fix it."
            )
            raise e
