#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*- author: sgt911 -*-

from server import make_app
from tornado.ioloop import IOLoop
from logging import getLogger
from os import environ as env

log = getLogger("server.run")

if __name__ == "__main__":
    production = env.get("PROD", "1") == "1"
    # Creating server
    app = make_app(not production)

    port = int(env.get("PORT", "8080"))

    # Enabling HTTP Server
    if production:
        from tornado.httpserver import HTTPServer

        server = HTTPServer(app)
        server.listen(port, "0.0.0.0")

        server.start(1)
    else:
        app.listen(port)
        log.warning("Do not run the debug mode on a production server")

    log.info("Server running on port %d", port)

    # IOLoop starting
    try:
        IOLoop.current().start()
    except KeyboardInterrupt:
        IOLoop.current().stop()
