from tornado.web import RequestHandler
from tornado.httpclient import AsyncHTTPClient, HTTPRequest, HTTPError
from tornado.ioloop import IOLoop
from tornado.escape import json_encode


class DiscordErrorReport(RequestHandler):
    _discord_webhook: str = None

    def set_discord_webhook(self, url: str):
        self._discord_webhook = url

    def _handle_request_exception(self, e: Exception) -> None:
        self.set_status(202)
        self.finish()

        logger = getattr(self, "log", None)
        if logger is not None:
            logger.error(f"{e.__class__.__name__}: {str(e)}")

        if self._discord_webhook is not None:
            import sys, traceback

            AsyncHTTPClient.configure("tornado.curl_httpclient.CurlAsyncHTTPClient")
            http_cli = AsyncHTTPClient(
                force_instance=True,
                defaults=dict(
                    user_agent="TelegramBot/v1.0.0-QueryMe",
                    raise_error=False,
                ),
            )

            err_cls, value, tb = sys.exc_info()
            if err_cls is not None:
                tb_str = "\n".join(traceback.format_tb(tb))
                req = HTTPRequest(
                    self._discord_webhook,
                    method="POST",
                    headers={
                        "Content-Type": "application/json",
                    },
                    body=json_encode(
                        {
                            "content": f"Telegram Bot raise `{err_cls.__name__}: {value}`\n\n```python\n{tb_str}```"
                        }
                    ),
                )

                async def wrapper(self, send_fx, req):
                    try:
                        await send_fx(req)
                    except HTTPError as e:
                        logger = getattr(self, "log", None)
                        if logger is not None:
                            logger.error(f"{e.__class__.__name__}: {str(e)}")
                            logger.error(e.response.body.decode())

                IOLoop.current().spawn_callback(wrapper, self, http_cli.fetch, req)
