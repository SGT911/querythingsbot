from urllib.parse import quote_plus as urlencode, unquote as urldecode
from functools import reduce
import ast, math
import operator as op


def build_query(**data: dict) -> str:
    def wp(acc, el):
        if isinstance(el[1], bytes):
            txt = el[0] + "=" + el[1].decode()
        else:
            txt = el[0] + "=" + urlencode(str(el[1]))
        if acc == "":
            return txt

        return acc + "&" + txt

    return reduce(wp, data.items(), "")


def eval_expr(expr):
    data = _eval_(ast.parse(expr, mode="eval").body)
    return str(data).upper()


def _eval_(node):
    vars = {
        "pi": math.pi,
        "tau": math.tau,
        "e": math.e,
        "TRUE": True,
        "FALSE": False,
    }

    funcs = {
        "sqrt": math.sqrt,
        "root": lambda b, n: n ** (1 / b),
        "fact": math.factorial,
        "pow": lambda x, y: x**y,
        "sin": math.sin,
        "sen": math.sin,
        "cos": math.cos,
        "tan": math.tan,
        "deg": math.degrees,
        "rad": math.radians,
    }

    operators = {
        ast.Add: op.add,
        ast.Sub: op.sub,
        ast.Mult: op.mul,
        ast.Div: op.truediv,
        ast.Pow: op.pow,
        ast.Mod: op.mod,
        ast.Eq: lambda x, y: x == y,
        ast.NotEq: lambda x, y: x != y,
        ast.Lt: lambda x, y: x > y,
        ast.LtE: lambda x, y: x >= y,
        ast.Gt: lambda x, y: x < y,
        ast.GtE: lambda x, y: x <= y,
    }

    if isinstance(node, ast.Call):  # <call>(<args>)
        if node.func.id in funcs:
            return funcs[node.func.id](*[_eval_(i) for i in node.args])
        raise ValueError(node.func.id)
    elif isinstance(node, ast.Name):  # <name>
        if node.id in vars:
            return vars[node.id]
        raise ValueError(node.id)
    elif isinstance(node, ast.Num):  # <number>
        return node.n
    elif isinstance(node, ast.Compare):  # <left> <ops> <comparators>
        return operators[type(node.ops[0])](
            _eval_(node.left), _eval_(node.comparators[0])
        )
    elif isinstance(node, ast.BinOp):  # <left> <operator> <right>
        return operators[type(node.op)](_eval_(node.left), _eval_(node.right))
    elif isinstance(node, ast.UnaryOp):  # <operator> <operand> e.g., -1
        return operators[type(node.op)](_eval_(node.operand))
    else:
        raise TypeError(node)
