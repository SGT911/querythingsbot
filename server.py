from tornado.web import Application, GZipContentEncoding
from logging import getLogger, basicConfig

from routes.webhook import WebhookHandler
from os import environ as env

_log = getLogger("server")
basicConfig(
    level="INFO",
    format="[%(asctime)s | %(levelname)s][%(name)s %(process)d]: %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S %Z",
)


def make_app(debug: bool = False) -> Application:
    bot_key = env.get("TL_KEY")
    discord_report = env.get("DISCORD_WH")

    lang_cache = dict()
    route_log = getLogger("server.routes")

    routes = [
        # fmt:off
        (r'^/$', WebhookHandler, dict(
            lang_cache=lang_cache,
            log=route_log,
            bot_key=bot_key,
            discord_report=discord_report,
        )),
        # fmt:on
    ]

    _log.debug("%d routes loaded", len(routes))

    return Application(
        routes,
        transforms=[GZipContentEncoding],
        autoreload=debug,
        debug=debug,
    )
